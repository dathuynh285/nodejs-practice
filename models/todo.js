const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const constants = require("../config/constants");
const TODO_STATUS = constants.TODO_STATUS;

var TodoSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "User",
  },
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
  },
  status: {
    type: String,
    enum: [TODO_STATUS.new, TODO_STATUS.doing, TODO_STATUS.done],
    default: TODO_STATUS.new,
  },
  createdAt: {
    type: Date,
  },
  updatedAt: {
    type: Date,
  },
});

module.exports = mongoose.model("Todo", TodoSchema);
