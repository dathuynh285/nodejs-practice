require("dotenv").config();

var connectionString = `mongodb://localhost:27017/nodejs_practice`;
if (process.env.NODE_ENV === "production")
  connectionString = `mongodb+srv://db_admin:MongoDB123456@cluster0.rytrb.mongodb.net/nodejs_practice`;

module.exports = {
  connectionString: connectionString,
};
