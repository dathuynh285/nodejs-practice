module.exports = {
  TODO_STATUS: {
    new: "new",
    doing: "doing",
    done: "done",
  },
};
