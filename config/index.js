const db = require("./database");
const auth = require("./auth");

module.exports = {
  db: db,
  auth: auth,
};
