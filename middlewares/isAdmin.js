const user = require("../models/user");
const utils = require("../utils/utils");

module.exports = async (req, res, next) => {
  var user = null;
  if (typeof req.decoded != typeof undefined) {
    user = req.decoded;
    if (user.role == "admin") {
      return next();
    }
  }
  return res.status(403).json({
    message: "Forbidden!!!",
  });
};
