const user = require("../models/user");
const utils = require("../utils/utils");

module.exports = async (req, res, next) => {
  // Get token in header of request
  const token = req.headers.token;

  // decode token
  if (token) {
    // verify token
    try {
      const decoded = await utils.verifyJwtToken(token, "config.auth.secret");
      // check user
      user.findById(decoded._id, (err, result) => {
        if (!result) {
          return res.status(401).json({
            message: "Unauthorized access.",
          });
        }
      });
      req.decoded = decoded;
      next();
    } catch (err) {
      // error
      console.error(err);
      return res.status(401).json({
        message: "Unauthorized access.",
      });
    }
  } else {
    // not found token
    return res.status(403).send({
      message: "No token provided.",
    });
  }
};
