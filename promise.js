const ping = require("web-pingjs");

function job1(num, callback = null) {
  console.log(`JOB1: ${num}`);
  if (callback) callback(num + 1);
  return num + 1;
}
function job2(num, callback = null) {
  console.log(`JOB2: ${num}`);
  if (callback) callback(num + 2);
  return num + 2;
}
function job3(num, callback = null) {
  console.log(`JOB3: ${num}`);
  if (callback) callback(num + 3);
  return num + 3;
}

job1(1, (start_num_2) => {
  job2(start_num_2, (start_num_3) => {
    job3(start_num_3);
  });
});

console.log("=====1=====");

var myjob = new Promise(function (resolve, reject) {
  resolve(1);
});

myjob
  .then((num) => job1(num))
  .then((num1) => job2(num1))
  .then((num2) => job3(num2));

setTimeout(() => {
  console.log("=====2=====");
  var myjob2 = Promise.resolve(1)
    .then((num) => job1(num))
    .then((num1) => job2(num1))
    .then((num2) => job3(num2));
}, 1);

setTimeout(() => {
  console.log("=====3=====");
  const myjob = async (num) => {
    try {
      const mjob1 = await job1(num);
      const mjob2 = await job2(mjob1);
      await job3(mjob2);
    } catch (err) {
      console.log(err);
    }
  };
  myjob(1);
}, 1);

setTimeout(() => {
  console.log("=====4=====");
  Promise.race([
    ping("google.com").then((res) => {
      console.log("1: " + res);
    }),
  ]).then((result) => {
    console.log(result);
  });
}, 1000);
