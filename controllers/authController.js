const jwt = require("jsonwebtoken");
const User = require("../models/user");
var config = require("../config");

class Controller {
  login(req, res) {
    User.findOne(
      {
        email: req.body.email,
      },
      function (err, user) {
        if (err) throw err;

        if (!user) {
          res.send({
            status: false,
            message: "Authentication failed. User not found.",
          });
        } else {
          // check if password matches
          user.comparePassword(req.body.password, function (err, isMatch) {
            if (isMatch && !err) {
              // if user is found and password is right create a token
              var token = jwt.sign(user.toJSON(), "config.auth.secret");
              // return the information including token as JSON
              res.json({
                status: true,
                token: token,
                user: { id: user._id, email: user.email, role: user.role },
              });
            } else {
              res.send({
                status: false,
                message: "Authentication failed. Wrong password.",
              });
            }
          });
        }
      }
    );
  }

  register(req, res) {
    User.create(
      {
        email: req.body.email,
        password: req.body.password,
      },
      (err, row) => {
        if (err) {
          if (err.code === 11000) {
            return res.json({
              status: false,
              message: "Email is taken, please choose other email.",
            });
          }
          return res.json(err.message);
        }
        return res.json(row);
      }
    );
  }
}

module.exports = Controller;
