class Controller {
  login(req, res) {
    res.render("login");
  }

  register(req, res) {
    res.render("register");
  }

  users(req, res) {
    var breadcrumbs = [
      {
        title: "Home",
        link: "/",
      },
      {
        title: "Users",
        link: "#",
      },
    ];
    res.render("users", { breadcrumbs: breadcrumbs });
  }

  todos(req, res) {
    var breadcrumbs = [
      {
        title: "Home",
        link: "/",
      },
      {
        title: "Todos",
        link: "#",
      },
    ];
    res.render("todos", { breadcrumbs: breadcrumbs });
  }
}

module.exports = new Controller();
