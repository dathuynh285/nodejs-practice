const Todo = require("../models/todo");
const User = require("../models/user");

class Controller {
  find(req, res) {
    Todo.find({ user: req.decoded._id }, (err, rows) => {
      if (err) throw err;
      res.json(rows);
    }).populate("user");
  }

  findOne(req, res) {
    if (!req.params.id) {
      res.status(500).send(":id is required!");
    } else {
      Todo.findOne(
        {
          _id: req.params.id,
        },
        (err, row) => {
          if (err) throw err;
          res.status(200).json(row);
        }
      ).populate("user");
    }
  }

  create(req, res) {
    var user = req.decoded;
    Todo.create(
      {
        title: req.body.title,
        content: req.body.content,
        user: user,
      },
      (err, row) => {
        if (err) {
          return res.json(err.message);
        }
        User.findById(user._id).then((usr) => {
          if (usr) {
            usr.todos.push(row._id);
            usr.save();
          }
        });
        return res.json(row);
      }
    );
  }

  update(req, res) {
    if (!req.params.id) {
      res.status(500).send(":id is required!");
    } else {
      Todo.findOne({ _id: req.params.id })
        .then((todo) => {
          todo
            .set(req.body)
            .save()
            .then((todo) => {
              return res.status(200).json(todo);
            })
            .catch((err) => {
              return res.status(500).json(err);
            });
        })
        .catch((err) => {
          return res.status(500).json(err);
        });
    }
  }

  remove(req, res) {
    if (!req.params.id) {
      res.status(500).send(":id is required!");
    } else {
      Todo.findOne({ _id: req.params.id })
        .then((todo) => {
          User.findOne({ _id: todo.user._id }).then((user) => {
            const index = user.todos.indexOf(todo._id);
            if (index > -1) {
              user.todos.splice(index, 1);
            }
            user.save();
          });
          todo.remove();
          return res.json({
            status: true,
            message: "Deleted successful",
          });
        })
        .catch((err) => {
          return res.json({
            status: false,
            message: "Not found todo!",
          });
        });
    }
  }
}

module.exports = Controller;
