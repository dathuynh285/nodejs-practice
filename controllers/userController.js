const User = require("../models/user");

class Controller {
  find(req, res) {
    User.find((err, rows) => {
      if (err) throw err;
      res.json(rows);
    }).populate("todo");
  }

  findOne(req, res) {
    if (!req.params.id) {
      res.status(500).send(":id is required!");
    } else {
      User.findOne(
        {
          _id: req.params.id,
        },
        (err, row) => {
          if (err) throw err;
          res.status(200).json(row);
        }
      ).populate("todo");
    }
  }

  create(req, res) {
    User.create(
      {
        email: req.body.email,
        password: req.body.password,
        role: req.body.role,
      },
      (err, row) => {
        if (err) {
          if (err.code === 11000) {
            return res.json({
              status: false,
              message: "Email is taken, please choose other email.",
            });
          }
          return res.json(err.message);
        }
        return res.json(row);
      }
    );
  }

  update(req, res) {
    if (!req.params.id) {
      res.status(500).send(":id is required!");
    } else {
      User.findOne({ _id: req.params.id })
        .then((user) => {
          user
            .set(req.body)
            .save()
            .then((user) => {
              return res.status(200).json(user);
            })
            .catch((err) => {
              if (err.code === 11000) {
                return res.json({
                  status: false,
                  message: "Email is taken, please choose other email.",
                });
              }
              return res.status(500).json(err);
            });
        })
        .catch((err) => {
          if (err.code === 11000) {
            return res.json({
              status: false,
              message: "Email is taken, please choose other email.",
            });
          }
          return res.status(500).json(err);
        });
    }
  }

  remove(req, res) {
    if (!req.params.id) {
      res.status(500).send(":id is required!");
    } else {
      User.deleteOne({ _id: req.params.id }, (err, row) => {
        if (err) throw err;
        res.json(row);
      });
    }
  }
}

module.exports = Controller;
