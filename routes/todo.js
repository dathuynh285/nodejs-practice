const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const Controller = require("../controllers/todoController");
const CheckTokenMiddleware = require("../middlewares/CheckTokenMiddleware");
const controller = new Controller();
const prefix = "/api/todos";

module.exports = (router) => {
  // Find
  router.get(prefix + "/", CheckTokenMiddleware, (req, res) =>
    controller.find(req, res)
  );

  // Find One
  router.get(
    prefix + "/:id",
    CheckTokenMiddleware,
    urlencodedParser,
    (req, res) => controller.findOne(req, res)
  );

  // Create
  router.post(prefix + "/", CheckTokenMiddleware, jsonParser, (req, res) =>
    controller.create(req, res)
  );

  // Update
  router.put(
    prefix + "/:id",
    CheckTokenMiddleware,
    urlencodedParser,
    jsonParser,
    (req, res) => controller.update(req, res)
  );

  // Delete
  router.delete(
    prefix + "/:id",
    CheckTokenMiddleware,
    urlencodedParser,
    (req, res) => controller.remove(req, res)
  );
};
