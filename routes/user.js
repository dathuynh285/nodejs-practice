const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const Controller = require("../controllers/userController");
const CheckTokenMiddleware = require("../middlewares/CheckTokenMiddleware");
const isAdmin = require("../middlewares/isAdmin");
const controller = new Controller();
const prefix = "/api/users";

module.exports = (router) => {
  // Find
  router.get(prefix + "/", CheckTokenMiddleware, isAdmin, (req, res) =>
    controller.find(req, res)
  );

  // Find One
  router.get(
    prefix + "/:id",
    CheckTokenMiddleware,
    isAdmin,
    urlencodedParser,
    (req, res) => controller.findOne(req, res)
  );

  // Create
  router.post(
    prefix + "/",
    CheckTokenMiddleware,
    isAdmin,
    jsonParser,
    (req, res) => controller.create(req, res)
  );

  // Update
  router.put(
    prefix + "/:id",
    CheckTokenMiddleware,
    isAdmin,
    urlencodedParser,
    jsonParser,
    (req, res) => controller.update(req, res)
  );

  // Delete
  router.delete(
    prefix + "/:id",
    CheckTokenMiddleware,
    isAdmin,
    urlencodedParser,
    (req, res) => controller.remove(req, res)
  );
};
