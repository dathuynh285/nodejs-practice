const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

const Controller = require("../controllers/authController");
const controller = new Controller();
const prefix = "/api/auth";

module.exports = (router) => {
  // Register
  router.post(prefix + "/register", jsonParser, (req, res) =>
    controller.register(req, res)
  );

  // Login
  router.post(prefix + "/login", jsonParser, (req, res) =>
    controller.login(req, res)
  );
};
