const router = require("express").Router();
const homeController = require("../controllers/homeController");
const pageController = require("../controllers/pageController");

router.get("/", (req, res) => homeController.index(req, res));

router.get("/login", (req, res) => pageController.login(req, res));
router.get("/register", (req, res) => pageController.register(req, res));

router.get("/users", (req, res) => pageController.users(req, res));
router.get("/todos", (req, res) => pageController.todos(req, res));

require("./user")(router);
require("./todo")(router);
require("./auth")(router);

module.exports = router;
