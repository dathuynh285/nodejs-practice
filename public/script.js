// const base_url = `http://localhost:1337/api`;
const base_url = `https://nodejs-practice-infodation.herokuapp.com/api`;

/**
 * USERS AREA EXECUTING
 */
function loadUsers() {
  axios
    .get(`${base_url}/users`, {
      headers: {
        token: localStorage.getItem("token"),
      },
    })
    .then((res) => {
      if (res.data) {
        var users = res.data;
        users.forEach((user) => {
          $("#tableUsers tbody").append(`
              <tr>
                  <td><strong>${user._id}</strong></td>
                  <td>${user.email}</td>
                  <td class="text-center">
                    <a href="javascript:;" data-id="${user._id}" class="btn btn-primary btn-sm btn-view">View</a>
                    <a href="javascript:;" data-id="${user._id}" class="btn btn-warning btn-sm btn-edit">Edit</a>
                    <a href="javascript:;" data-id="${user._id}" class="btn btn-danger btn-sm btn-delete">Delete</a>
                  </td>
              </tr>
            `);
        });
      }
      initButtonActions();
    })
    .catch((err) => {
      if (err.response.status == 401) window.location.href = "/login";
      else
        swal({
          title: "Nope!",
          text: err.response.data.message,
          icon: "error",
        });
    });
}

function initButtonActions() {
  $(".btn-view").on("click", (e) => {
    let element = $(e)[0].target;
    let id = $(element).data("id");

    axios
      .get(`${base_url}/users/${id}`, {
        headers: {
          token: localStorage.getItem("token"),
        },
      })
      .then((res) => {
        var user_info = res.data;
        $("#userInfoModal .modal-body").html(`
          <ul>
              <li><strong>ID:</strong> ${user_info._id}</li>
              <li><strong>Email:</strong> ${user_info.email}</li>
          </ul>
        `);
        $("#userInfoModal").modal("show");
      })
      .catch((err) => {
        swal({
          title: "Nope!",
          text: "Crashed!",
          icon: "error",
        });
      });
  });

  $(".btn-edit").on("click", (e) => {
    let element = $(e)[0].target;
    let id = $(element).data("id");

    axios
      .get(`${base_url}/users/${id}`, {
        headers: {
          token: localStorage.getItem("token"),
        },
      })
      .then((res) => {
        var user_info = res.data;
        $("#contentModalLabel").text(`Update: ${user_info._id}`);
        $("#inputID").val(user_info._id);
        $("#inputEmail").val(user_info.email);
        $("#inputPassword").val("");
        $("#inputRole").val(user_info.role);
        $("#btnSubmit").val("update");
        $("#contentModal").modal("show");
      })
      .catch((err) => {
        swal({
          title: "Nope!",
          text: "Crashed!",
          icon: "error",
        });
      });
  });

  $(".btn-delete").on("click", (e) => {
    let element = $(e)[0].target;
    let id = $(element).data("id");

    if (id == localStorage.getItem("user_id")) {
      alert("Cannot delete yourself!");
    } else {
      swal({
        title: "Are you sure delete user?",
        text: "You will not be able to recover this data!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          axios
            .delete(`${base_url}/users/${id}`, {
              headers: {
                token: localStorage.getItem("token"),
              },
            })
            .then((res) => {
              swal({
                title: "Deleted!",
                text: "User is removed!",
                icon: "success",
                buttons: false,
              });
              setTimeout(() => {
                window.location.reload();
              }, 1700);
            });
        }
      });
    }
  });
}

$("#btnCreate").on("click", (e) => {
  $("#contentModalLabel").text(`Create user`);
  $("#inputEmail").val("");
  $("#inputPassword").val("");
  $("#btnSubmit").val("create");
  $("#contentModal").modal("show");
});

$("#btnSubmit").on("click", (e) => {
  if ($("#inputEmail").val() == "") {
    alert("Please type an email!");
  } else if (
    $("#inputPassword").val() == "" &&
    $("#btnSubmit").val() == "create"
  ) {
    alert("Please type password!");
  } else if ($("#btnSubmit").val() == "create") {
    axios
      .post(
        `${base_url}/users`,
        {
          email: $("#inputEmail").val(),
          password: $("#inputPassword").val(),
          role: $("#inputRole").val(),
        },
        {
          headers: {
            token: localStorage.getItem("token"),
          },
        }
      )
      .then((res) => {
        if (res.data.status === false) {
          alert(res.data.message);
        } else {
          swal({
            title: "Created!",
            text: "User is created successful!",
            icon: "success",
            buttons: false,
          });
          setTimeout(() => {
            window.location.reload();
          }, 1700);
        }
      })
      .catch((err) => {
        alert(err.response.data.message);
      });
  } else if ($("#btnSubmit").val() == "update") {
    var _id = $("#inputID").val();
    var dataToUpdate = {
      email: $("#inputEmail").val(),
      role: $("#inputRole").val(),
    };
    if ($("#inputPassword").val() != "")
      dataToUpdate.password = $("#inputPassword").val();

    axios
      .put(`${base_url}/users/${_id}`, dataToUpdate, {
        headers: {
          token: localStorage.getItem("token"),
        },
      })
      .then((res) => {
        if (res.data.status === false) {
          alert(res.data.message);
        } else {
          swal({
            title: "Updated!",
            text: "User is updated successful!",
            icon: "success",
            buttons: false,
          });
          setTimeout(() => {
            window.location.reload();
          }, 1700);
        }
      });
  }
});

/**
 * AUTH AREA EXECUTING
 */
$("#formLogin").on("submit", (e) => {
  e.preventDefault();
  var formData = {
    email: $('#formLogin [name="email"]').val(),
    password: $('#formLogin [name="password"]').val(),
  };

  axios
    .post(`${base_url}/auth/login`, formData, {
      headers: {
        token: localStorage.getItem("token"),
      },
    })
    .then((res) => {
      if (res.data.status === false) {
        alert(res.data.message);
      } else {
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("user_id", res.data.user.id);
        localStorage.setItem("user_email", res.data.user.email);
        localStorage.setItem("user_role", res.data.user.role);
        swal({
          title: "Success!",
          text: "Logined successful!",
          icon: "success",
          buttons: false,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1700);
      }
    })
    .catch((err) => {
      alert("Error!!!");
    });
});

$("#formRegister").on("submit", (e) => {
  e.preventDefault();

  const email = $("#inputEmail").val();
  const password = $("#inputPassword").val();
  const confirmPassword = $("#inputConfirmPassword").val();

  if (email == "") {
    alert("Please type an email!");
  } else if (password == "") {
    alert("Please type password!");
  } else if (confirmPassword == "") {
    alert("Please confirm password!");
  } else if (confirmPassword != password) {
    alert("Two passwords not same!");
  } else {
    axios
      .post(
        `${base_url}/auth/register`,
        {
          email: email,
          password: password,
        },
        {
          headers: {
            token: localStorage.getItem("token"),
          },
        }
      )
      .then((res) => {
        if (res.data.status === false) {
          alert(res.data.message);
        } else {
          swal({
            title: "Success!",
            text: "Register successful!",
            icon: "success",
            buttons: false,
          });
          setTimeout(() => {
            window.location.href = "/login";
          }, 1700);
        }
      })
      .catch((err) => {
        alert("Error!!!");
      });
  }
});

function initAuthArea() {
  if (localStorage.getItem("user_id") && localStorage.getItem("user_email")) {
    var email = localStorage.getItem("user_email");
    $("#authArea").html(`
      <li class="nav-item">
          <a class="nav-link text-default" href="javascript:;">${email}</a>
      </li>
      <li class="nav-item">
          <a class="nav-link text-danger" id="btnLogout" href="javascript:;">Logout</a>
      </li>
    `);
  } else {
    $("#authArea").html(`
      <li class="nav-item">
          <a class="nav-link text-success" href="/login">Login</a>
      </li>
      <li class="nav-item">
          <a class="nav-link text-primary" href="/register">Register</a>
      </li>
    `);
  }
}

/**
 * TODO AREA EXECUTING
 */
$("#btnCreateTodo").on("click", (e) => {
  $("#contentModalLabel").text(`Create todo`);
  $("#inputTitle").val("");
  $("#inputContent").val("");
  $("#btnSubmit").val("create");

  $("#contentModal").modal("show");
});

$("#btnSubmitTodo").on("click", (e) => {
  $("#inputContent").val(nicEditors.findEditor("inputContent").getContent());
  // console.log(content.getContent());
  if ($("#inputTitle").val() == "") {
    alert("Please type title!");
  } else if ($("#inputContent").val() == "") {
    alert("Please type content!");
  } else if ($("#btnSubmitTodo").val() == "create") {
    axios
      .post(
        `${base_url}/todos`,
        {
          title: $("#inputTitle").val(),
          content: $("#inputContent").val(),
        },
        {
          headers: {
            token: localStorage.getItem("token"),
          },
        }
      )
      .then((res) => {
        if (res.data.status === false) {
          alert(res.data.message);
        } else {
          swal({
            title: "Created!",
            text: "Todo is created successful!",
            icon: "success",
            buttons: false,
          });
          setTimeout(() => {
            window.location.reload();
          }, 1700);
        }
      })
      .catch((err) => {
        alert("Error!!!");
      });
  } else if ($("#btnSubmitTodo").val() == "update") {
    var _id = $("#inputID").val();
    var dataToUpdate = {
      title: $("#inputTitle").val(),
      content: $("#inputContent").val(),
    };
    axios
      .put(`${base_url}/todos/${_id}`, dataToUpdate, {
        headers: {
          token: localStorage.getItem("token"),
        },
      })
      .then((res) => {
        if (res.data.status === false) {
          alert(res.data.message);
        } else {
          swal({
            title: "Updated!",
            text: "Todo is updated successful!",
            icon: "success",
            buttons: false,
          });
          setTimeout(() => {
            window.location.reload();
          }, 1700);
        }
      });
  }
});

function loadTodos() {
  axios
    .get(`${base_url}/todos`, {
      headers: {
        token: localStorage.getItem("token"),
      },
    })
    .then((res) => {
      if (res.data) {
        var todos = res.data;
        // console.log(todos);
        todos.forEach((todo) => {
          $("#tableTodos tbody").append(`
              <tr>
                  <td><strong>${todo._id}</strong></td>
                  <td>${todo.title}</td>
                  <td class="text-center">
                    <a href="javascript:;" data-id="${todo._id}" class="btn btn-primary btn-sm btn-view">View</a>
                    <a href="javascript:;" data-id="${todo._id}" class="btn btn-warning btn-sm btn-edit">Edit</a>
                    <a href="javascript:;" data-id="${todo._id}" class="btn btn-danger btn-sm btn-delete">Delete</a>
                  </td>
              </tr>
            `);
        });
      }
      initButtonActionsTodo();
    })
    .catch((err) => {
      if (err.response.status == 401) window.location.href = "/login";
      else
        swal({
          title: "Nope!",
          text: err.response.data.message,
          icon: "error",
        });
    });
}

function initButtonActionsTodo() {
  $(".btn-view").on("click", (e) => {
    let element = $(e)[0].target;
    let id = $(element).data("id");

    axios
      .get(`${base_url}/todos/${id}`, {
        headers: {
          token: localStorage.getItem("token"),
        },
      })
      .then((res) => {
        var todo = res.data;
        $("#infoModal .modal-body").html(`
          <ul>
              <li><strong>ID:</strong> ${todo._id}</li>
              <li><strong>Title:</strong> ${todo.title}</li>
              <li><strong>Content:</strong></li>
          </ul>
          <p>${todo.content}</p>
        `);
        $("#infoModal").modal("show");
      })
      .catch((err) => {
        swal({
          title: "Nope!",
          text: "Crashed!",
          icon: "error",
        });
      });
  });

  $(".btn-edit").on("click", (e) => {
    let element = $(e)[0].target;
    let id = $(element).data("id");

    axios
      .get(`${base_url}/todos/${id}`, {
        headers: {
          token: localStorage.getItem("token"),
        },
      })
      .then((res) => {
        var todo = res.data;
        $("#contentModalLabel").text(`Update: ${todo._id}`);
        $("#inputID").val(todo._id);
        $("#inputTitle").val(todo.title);
        $("#inputContent").val(todo.content);

        nicEditors.findEditor("inputContent").setContent(todo.content);

        $("#btnSubmitTodo").val("update");
        $("#contentModal").modal("show");
      })
      .catch((err) => {
        swal({
          title: "Nope!",
          text: "Crashed!",
          icon: "error",
        });
      });
  });

  $(".btn-delete").on("click", (e) => {
    let element = $(e)[0].target;
    let id = $(element).data("id");

    swal({
      title: "Are you sure delete user?",
      text: "You will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        axios
          .delete(`${base_url}/todos/${id}`, {
            headers: {
              token: localStorage.getItem("token"),
            },
          })
          .then((res) => {
            swal({
              title: "Deleted!",
              text: "Todo is removed!",
              icon: "success",
              buttons: false,
            });
            setTimeout(() => {
              window.location.reload();
            }, 1700);
          });
      }
    });
  });
}

/**
 * EXECUTING ON DOCUMENT LOADING
 */
$(document).ready(() => {
  initAuthArea();

  bkLib.onDomLoaded(() => {
    new nicEditor({ fullPanel: true }).panelInstance("inputContent", {
      hasPanel: true,
    });
  });

  $("#btnLogout").on("click", (e) => {
    swal({
      title: "Are you sure logout?",
      text: "You will be logout!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((ok) => {
      if (ok) {
        localStorage.clear();
        window.location.reload();
      }
    });
  });
});
