require("dotenv").config();
var express = require("express");
var mongoose = require("mongoose");
var config = require("./config");
const router = require("./routes");

var app = express();
const port = process.env.PORT || 3000; // app port

/**
 * MongoDB connection
 */
mongoose.connect(config.db.connectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

/**
 * MIDDLEWARES
 */
app.use("/assets", express.static(__dirname + "/public")); // static files
app.set("view engine", "ejs");

// Log request
app.use("/", (req, res, next) => {
  console.log(`[${new Date().toDateString()}] ${req.method}: ${req.url}`);
  next();
});

app.use(router);

/**
 * START APP
 */

app.listen(port, () => {
  console.log(`Server is running on : http://localhost:${port}`);
});
